import { filterIntegration, convertJsonInCSV, getAllResults } from "./services.js";

/*preencha esse array com as notas que deseja gerar o relatório*/
let arrayNotas = [ ];

let relatorioJson = await getAllResults(arrayNotas);

convertJsonInCSV(relatorioJson);

/** Para buscar com um filtro basta descomentar as linhas abaixo e definir a integração e o status do filtro de busca*/ 

/* let search = {
    integracao: "guepardo",
    status: "Sucesso"
}

const resultFilter = filterIntegration(relatorioJson, search.integracao, search.status);

convertJsonInCSV(resultFilter);

*/

