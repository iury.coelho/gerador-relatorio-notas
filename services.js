import axios from "axios";
import json2csv from "json2csv"
import fs from "fs"
import { END_POINT_INTHUB } from "./config.js";

async function fetchData(identifier, systemType) {
    let integrationNumber;

    switch (systemType) {
        case "sap":
            integrationNumber = 1;
            break;
        case "guepardo":
            integrationNumber = 2;
            break;
        case "linx":
            integrationNumber = 3;
            break;
        default:
            throw new Error("Invalid system type");
    }

    const params = {
        pageNumber: 0,
        pageSize: 100,
        identifier,
        integrationId: integrationNumber,
    };

    try {
        const response = await axios.get(END_POINT_INTHUB, { params });
        return response
    } catch (error) {
        console.error(error);
    }
}

async function getAllResults(arrayNotas){

    let integracoes = ["linx", "guepardo", "sap"];
    let arrayResultados = [];

    for (let i = 0; i < integracoes.length; i++) {
        for (let n = 0; n < arrayNotas.length; n++) {
            let responseAtual = await fetchData(arrayNotas[n], integracoes[i]);
            if (responseAtual === undefined) {
                console.error("responseAtual é undefined, impossível ler suas propriedades. Você está logado na VPN? ");
                break;
            }
            if ((responseAtual['data']['content']).length != 0) {
                const identifierResponse = responseAtual['data']['content'][0]['identifier'];
                const statusResponse = responseAtual['data']['content'][0]['status'];
                let resultado = {
                    identifier: identifierResponse,
                    status: statusResponse,
                    integracao: integracoes[i]
                };
                arrayResultados.push(resultado);
            }
    
        }
    }

    return arrayResultados;   

}

function filterIntegration(array, integration, status) {
    return array.filter(function (item) {
        return item.integracao === integration && item.status === status;        
    });
}


function convertJsonInCSV(arrayObjt){
    try {
        const currentDate = new Date();
        const dateString = currentDate.toLocaleDateString().split("/").join("");
        const timeString = currentDate.toLocaleTimeString().split(":").join("");
    
        const csv = json2csv.parse(arrayObjt);
        fs.writeFileSync(`./relatorio-notas-${dateString}-${timeString}.csv`, csv);
        console.log(`Arquivo CSV salvo com sucesso em ${dateString}-${timeString}.`);
    } catch (err) {
        console.error(err);
    }    

}

export { filterIntegration, convertJsonInCSV, getAllResults }








