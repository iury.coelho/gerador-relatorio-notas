# gerador-relatorio-notas

Este script é utilizado para coletar informações de integrações de notas fiscais e armazená-las em um arquivo CSV. Ele faz uma requisição HTTP para a API Inthub, filtra os resultados de acordo com as informações desejadas e os armazena em um arquivo CSV.

## Bibliotecas utilizadas

- Dependências:

1. `axios`: biblioteca para realizar requisições HTTP.
2. `json2csv`: biblioteca para conversão de dados JSON para CSV.
3. `fs`: biblioteca do Node.js para manipulação de arquivos.

- Dependências de desenvolvimento:

1. `nodemon`: ferramenta para monitoramento automático de mudanças no código fonte e reinicialização do servidor.

## Variáveis e constantes

`baseURL`: é uma constante que contém a URL base da API Inthub.

`arrayNotas`: é um array que contém identificadores de notas fiscais para serem consultados na API.

`search`: é um objeto que contém as informações de integração e status que serão usadas para filtrar os resultados.

`arrayResultados`: é um array que armazenará os resultados das consultas à API.

`integracoes`: é um array que contém os tipos de integrações disponíveis para serem consultados na API.


## Funções

`fetchData`: é uma função assíncrona que faz uma requisição HTTP para a API Inthub com base no identificador de nota fiscal e no tipo de integração informados como parâmetros.

`filterIntegration`: é uma função que filtra os resultados do arrayResultados com base nas informações de integração e status fornecidas como parâmetros.

`convertJsonInCSV`: é a função responsável por converter um objeto JSON em um arquivo CSV.

`getAllResults`: é responsável por buscar todos os resultados presentes no array de notas.


## Processamento

1. O script inicializa as variáveis e constantes.

2. O loop for é executado para cada tipo de integração contido no array integracoes.

3. Para cada tipo de integração, o loop for é executado para cada identificador de nota fiscal contido no arrayNotas.

4. A função fetchData é chamada para cada combinação de identificador e tipo de integração, retornando uma resposta da API.

5. Se a resposta da API não for vazia, os dados de identificador, status e tipo de integração são armazenados no arrayResultados.

6. A função filterIntegration é chamada para filtrar os resultados do arrayResultados com base nas informações de integração e status informadas na variável search.

7. O resultado filtrado é armazenado em uma nova variável, chamada filteredResults.

6. A função writeToCSV é chamada para salvar o resultado filtrado em um arquivo CSV.

7. Após o término do loop, o script exibe uma mensagem informando o número de registros processados e salvos no arquivo CSV.

8. O script também pode exibir uma mensagem de erro, caso algum erro tenha ocorrido durante o processamento.

## Diagrama de Fluxo de Processamento

<p align="center">
  <img src="diagrama_fluxo.png">
</p>



## Execução do Script
Ao executar o script, o mesmo irá buscar as informações sobre as notas fiscais que estão presentes no array arrayNotas para todos os tipos de integração presentes no array integracoes (que são "linx", "guepardo" e "sap").

As informações retornadas serão armazenadas no array arrayResultados. Em seguida, será realizado um filtro desses dados usando a função filterIntegration para buscar somente as informações que tenham a integração igual a search.integracao e o status igual a search.status.

Por fim, os dados filtrados serão salvos em um arquivo CSV, com o nome data-ddmmYYYY-HHMMSS.csv.

Para executar o script com o npm start-dev, siga os seguintes passos:

1. Abra o terminal ou o prompt de comando em seu computador.

2.  clone  o repositório e  navegue até a pasta raiz do projeto.

3. Execute o seguinte comando para instalar as dependências: `npm install`

4. Quando as dependências estiverem instaladas, execute o seguinte comando para iniciar o script em modo de desenvolvimento: `npm run start-dev`.

5. O script será iniciado e as informações serão integradas entre os sistemas, conforme descrito na descrição inicial do script.

6. Quando o processamento estiver concluído, os resultados serão salvos em um arquivo CSV, como mencionado na descrição do script.

7. Você pode verificar os resultados abrindo o arquivo CSV com um editor de planilhas ou acessando os dados diretamente no arquivo.

## Exemplo de Caso de Uso

1. Importar as funções necessárias do arquivo de serviços.js:

```js
import { filterIntegration, convertJsonInCSV, getAllResults } from "./services.js";

```

2. Inicializar o array de notas:

```js
let arrayNotas = [];

```

3. Obter todos os resultados através da função getAllResults passando o array de notas como parâmetro:

```js
let relatorioJson = await getAllResults(arrayNotas);

```

4. Converter o resultado em formato JSON para CSV:

```js
convertJsonInCSV(relatorioJson);
```


5. (Opcional) Definir as configurações de filtro de busca:

```js
let search = {
    integracao: "guepardo",
    status: "Sucesso"
}

```

6. (Opcional) Aplicar o filtro de busca utilizando a função filterIntegration passando como parâmetros o resultado em formato JSON, a integração e o status desejados:

```js
  const resultFilter = filterIntegration(relatorioJson, search.integracao, search.status);

```

7. (Opcional) Converter o resultado filtrado para o formato CSV:

```js
  convertJsonInCSV(resultFilter);

```

8. Observar o resultado final na forma de um arquivo CSV.
